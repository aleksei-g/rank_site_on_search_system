import csv


def get_dict_words_position_from_csv(filepath):
    words_position = {}
    with open(filepath) as file_handler:
        for i in range(2):
            file_handler.__next__()
        reader = csv.DictReader(file_handler, delimiter=';')
        for line in reader:
            line_dict = dict(line)
            words_position[line_dict.pop('Запрос')] = line_dict
    return words_position
