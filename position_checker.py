import os
import sys
import time
import re
from datetime import datetime
from urllib.parse import urlencode
from pathlib import Path

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from decorators import check_execution


class PositionChecker:
    _URL = 'https://be1.ru/position-yandex-google/'
    _DOWNLOAD_PATH = Path(os.path.dirname(sys.argv[0])).resolve() / 'downloads'
    _WAIT_LOAD_PAGE_SEC = 600
    _WAIT_LOAD_FILE_SEC = 60
    _ENGINES = 'yandex'
    _EXTEN_DOWNLOAD_FILE = '.csv'

    def __init__(self, download_path=_DOWNLOAD_PATH):
        self._download_path = Path(download_path or self._DOWNLOAD_PATH)
        _prefs = {"download.default_directory": str(self._download_path)}
        _chrome_options = webdriver.ChromeOptions()
        _chrome_options.add_experimental_option("prefs", _prefs)
        self._display = Display(visible=0, size=(800, 600))
        self._display.start()
        self._driver = webdriver.Chrome(chrome_options=_chrome_options)
        self._wait = WebDriverWait(self._driver, self._WAIT_LOAD_PAGE_SEC)

    @staticmethod
    def _get_file_extension(filepath):
        if not filepath:
            return
        return filepath.suffix

    @staticmethod
    def _get_site_name_from_url(url):
        site_name = re.compile(r"https?://(www\.)?")
        return site_name.sub('', url).strip().strip('/')

    @staticmethod
    def _build_url(url, url_params):
        return f"{url}?{urlencode(url_params)}"

    def _rename_file(self, filepath, code_region, url_check, part_num):
        name_str = (f"{datetime.now():%Y-%m-%d_%H-%M-%S}_"
                    f"{self._get_site_name_from_url(url_check)}_"
                    f"{code_region}_p{part_num}"
                    f"{filepath.suffix}")
        new_filepath = filepath.with_name(name_str)
        filepath.replace(new_filepath)
        return new_filepath

    def _get_filepath_to_newest_file(self):
        if not self._download_path.exists():
            return
        _, newest_file = max(
            ((f.stat().st_mtime, f) for f in self._download_path.iterdir()),
            default=(None, None),
        )
        return newest_file

    @check_execution()
    def get_csv_file_with_position(self, url_check, code_region, name_region,
                                   words_list, part_num):
        url = self._URL
        self._driver.get(url)
        textarea_queries = self._driver.find_element_by_name('queries')
        textarea_queries.clear()
        for word in words_list:
            textarea_queries.send_keys(f"{word}\r\n")
        btn_region_dropdown = self._driver.find_element_by_xpath(
            '//button[@class="btn dropdown-toggle btn-default"]')
        btn_region_dropdown.click()
        ul_region = self._driver.find_element_by_xpath(
            '//div[@class="dropdown-menu open"]'
            '//ul[@class="dropdown-menu inner"]')
        li_region = ul_region.find_element_by_xpath(
            '//li[a[.//span[text()="{}"]]]'.format(name_region)
        )
        li_region.location_once_scrolled_into_view
        li_region.click()
        input_url = self._driver.find_element_by_name('url')
        input_url.send_keys(url_check)
        btn_check = self._driver.find_element_by_xpath(
            '//button[@class="btn2"]')
        btn_check.click()
        success_btn = self._wait.until(EC.presence_of_element_located(
            (By.XPATH, '//button[@onclick="TOOL.saveCSV()"]')))
        success_btn.click()
        modal_dialog = self._wait.until(EC.visibility_of_element_located(
            (By.ID, 'csvModal')))
        download_btn = modal_dialog.find_element_by_class_name('btn-success')
        newest_file = self._get_filepath_to_newest_file()
        download_btn.click()
        loaded_file = self._get_filepath_to_newest_file()
        loaded_file_exten = self._get_file_extension(loaded_file)
        wait_sec = 0
        while ((newest_file == loaded_file
                or loaded_file_exten != self._EXTEN_DOWNLOAD_FILE)
               and wait_sec < self._WAIT_LOAD_FILE_SEC):
            time.sleep(1)
            wait_sec += 1
            loaded_file = self._get_filepath_to_newest_file()
            loaded_file_exten = self._get_file_extension(loaded_file)
        else:
            if wait_sec == self._WAIT_LOAD_FILE_SEC:
                raise Exception('File not loaded!')
        loaded_file = self._rename_file(loaded_file, code_region, url_check,
                                        part_num)
        return loaded_file

    def quit(self):
        self._driver.quit()
        self._display.stop()
