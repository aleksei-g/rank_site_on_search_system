from datetime import datetime
from pathlib import Path

from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Font
from openpyxl.utils.cell import get_column_letter
from openpyxl.worksheet.table import Table, TableStyleInfo


def convert_list_of_lists_to_list(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]


def create_parent_path(filepath):
    if not filepath.parent.exists():
        filepath.parent.mkdir()


class ExcelBook:
    _DEFAULT_TITLE_SHEET = 'Sheet'
    _HEADERS = [('№', 'Запрос')]
    _LIST_COUNT_IN_TOP_X = [
        (1, 'ТОП-1'), (5, 'ТОП-5'), (10, 'ТОП-10'), (20, 'ТОП-20'),
        (30, 'ТОП-30'), (40, 'ТОП-40'), (50, 'ТОП-50'), (100, 'ТОП-100'),
        ('-', 'Отсутствует'),
    ]
    _ROW_START_HEADER = 1
    _COUNT_HEADER_ROW = len(_HEADERS)
    _INDENT_BETWEEN_HEADER_AND_WORDS = 1
    _ROW_START_WORDS = (_ROW_START_HEADER + _COUNT_HEADER_ROW - 1 +
                        len(_LIST_COUNT_IN_TOP_X)
                        + _INDENT_BETWEEN_HEADER_AND_WORDS) + 1
    _COLUMN_NUMBER = 1
    _COLUMN_WORDS = 2
    _WIDTH_COLUMN_NUMBER = 3.7109375
    _WIDTH_COLUMN_WORDS = 36.7109375
    _WIDTH_COLUMN_POSITION = 5.7109375
    _UNDEFINED_WORD_POSITION = '-'
    _DEFAULT_TEXT_COLOR = 'FF000000'
    _DEFAULT_TABLE_STYLE = TableStyleInfo(
        name='TableStyleLight16',
        showFirstColumn=False,
        showLastColumn=False,
        showRowStripes=True,
        showColumnStripes=False,
    )

    def __init__(self, filepath, sheets):
        self.filepath = Path(filepath)
        self.sheets = sheets
        self._new_file = True
        if self.filepath.exists():
            self.wb = load_workbook(self.filepath)
            self._new_file = False
        else:
            self.wb = Workbook()
        self._sheets_init()

    def _sheets_init(self):
        for sheet_name in self.sheets:
            if self._new_file:
                sheet = self._create_sheet(sheet_name['name'])
                sheet.column_dimensions[
                    get_column_letter(self._COLUMN_NUMBER)].width = (
                    self._WIDTH_COLUMN_NUMBER)
                sheet.column_dimensions[
                    get_column_letter(self._COLUMN_WORDS)].width = (
                    self._WIDTH_COLUMN_WORDS)
                self._add_headers_to_table(sheet)
                self._set_column_font_for_top_x_block(sheet, self._COLUMN_WORDS)
            else:
                sheet = self.wb[sheet_name['name']]
            self._add_words_to_table(sheet, sheet_name['words'])

    def _create_sheet(self, sheet_name):
        if self._new_file and self.wb.active.title == self._DEFAULT_TITLE_SHEET:
            sheet = self.wb.active
            sheet.title = sheet_name
        elif sheet_name in self.wb.sheetnames:
            sheet = self.wb[sheet_name]
        else:
            sheet = self.wb.create_sheet(sheet_name)
        return sheet

    def _add_headers_to_table(self, sheet):
        headers = [
            *self._HEADERS,
            *[('', item[1]) for item in self._LIST_COUNT_IN_TOP_X],
        ]
        for row in headers:
            sheet.append(row)

    def _add_words_to_table(self, sheet, words):
        exist_words = self._get_list_values_from_range(
            sheet,
            min_col=self._COLUMN_WORDS,
            max_col=self._COLUMN_WORDS,
            min_row=self._ROW_START_WORDS,
            max_row=sheet.max_row,
        )
        exist_words = convert_list_of_lists_to_list(exist_words)
        new_words = sorted(set(words).difference(set(exist_words)))
        for word in new_words:
            formula = (f"=ROW() - ROW(${get_column_letter(self._COLUMN_NUMBER)}"
                       f"${self._ROW_START_WORDS - 1})")
            row = max(sheet.max_row + 1, self._ROW_START_WORDS)
            sheet.cell(row=row, column=self._COLUMN_NUMBER).value = formula
            sheet.cell(row=row, column=self._COLUMN_WORDS).value = word

    @staticmethod
    def _get_list_values_from_range(sheet, range_string=None,
                                    min_row=None, max_row=None, min_col=None,
                                    max_col=None, row_offset=0,
                                    column_offset=0):
        list_values = []
        for row_cells in sheet.iter_rows(range_string, min_row, max_row,
                                         min_col, max_col, row_offset,
                                         column_offset):
            row_values = []
            for cell in row_cells:
                row_values.append(cell.value)
            list_values.append(row_values)
        return list_values

    def _get_row_by_value(self, sheet, search_text):
        cur_row = 1
        while cur_row <= sheet.max_row:
            if sheet.cell(row=cur_row,
                          column=self._COLUMN_WORDS).value == search_text:
                return cur_row
            cur_row += 1
        return None

    def _write_column_name_for_region(self, sheet, row, column, region_name):
        headers = self._get_list_values_from_range(
            sheet,
            min_col=sheet.min_column,
            max_col=sheet.max_column,
            min_row=self._ROW_START_HEADER,
            max_row=self._ROW_START_HEADER + self._COUNT_HEADER_ROW - 1,
        )
        headers = convert_list_of_lists_to_list(headers)
        column_name = f"{datetime.today():%d.%m.%Y} {region_name}"
        base_column_name = column_name
        count_duplicate = 0
        while column_name in headers:
            count_duplicate += 1
            column_name = f"{base_column_name} #{count_duplicate}"
        sheet.cell(row=row, column=column, value=column_name)

    def _write_formula_count_words_in_top_x(self, sheet, column):
        start_cell = f"{get_column_letter(column)}{self._ROW_START_WORDS}"
        end_cell = (f"{get_column_letter(column)}"
                    f"{max(sheet.max_row, self._ROW_START_WORDS)}")
        for count_in_top_x in self._LIST_COUNT_IN_TOP_X:
            row = self._get_row_by_value(sheet, count_in_top_x[1])
            if row is None:
                continue
            if type(count_in_top_x[0]) == int:
                formula = (
                    f"=COUNTIFS({start_cell}:{end_cell},\">0\","
                    f"{start_cell}:{end_cell},\"<={count_in_top_x[0]}\")")
            else:
                formula = (f"=COUNTIFS({start_cell}:{end_cell},"
                           f"\"{self._UNDEFINED_WORD_POSITION}\")")
            sheet.cell(row=row, column=column, value=formula)

    def _set_column_style(self, sheet, column, color_text=_DEFAULT_TEXT_COLOR):
        sheet.column_dimensions[
            get_column_letter(column)].width = self._WIDTH_COLUMN_POSITION
        sheet.cell(row=1, column=column).alignment = Alignment(
            horizontal='general', wrapText=True)
        self._set_column_font_for_words_block(sheet, column, color_text)
        self._set_column_font_for_top_x_block(sheet, column, color_text)

    def _set_column_font_for_top_x_block(self, sheet, column,
                                         text_color=_DEFAULT_TEXT_COLOR):
        row_end_top_x_block = (self._ROW_START_HEADER + self._COUNT_HEADER_ROW -
                               1 + len(self._LIST_COUNT_IN_TOP_X))
        cells_range = (
            f"{get_column_letter(column)}{sheet.min_row}"
            f":{get_column_letter(column)}"
            f"{row_end_top_x_block}"
        )
        font = Font(bold=True, color=text_color)
        self._set_font_for_cells_range(sheet, cells_range, font)

    def _set_column_font_for_words_block(self, sheet, column,
                                         color_text=_DEFAULT_TEXT_COLOR):
        cells_range = (
            f"{get_column_letter(column)}{self._ROW_START_WORDS}"
            f":{get_column_letter(column)}"
            f"{sheet.max_row}"
        )
        font = Font(color=color_text)
        self._set_font_for_cells_range(sheet, cells_range, font)

    @staticmethod
    def _set_font_for_cells_range(sheet, cells_range, font):
        cells = sheet[cells_range]
        for row_list in cells:
            for cell in row_list:
                cell.font = font

    @staticmethod
    def _set_table_style(sheet, style=_DEFAULT_TABLE_STYLE):
        res_table = Table(
            displayName=f"{sheet.title}Style",
            ref=f"{get_column_letter(sheet.min_column)}{sheet.min_row}"
                f":{get_column_letter(sheet.max_column)}{sheet.max_row}",
        )
        res_table.tableStyleInfo = style
        if len(sheet._tables):
            sheet._tables[0] = res_table
        else:
            sheet._tables.append(res_table)

    def _freeze_panes(self, sheet):
        row_freeze = self._COUNT_HEADER_ROW + 1
        column_freeze = self._COLUMN_WORDS + 1
        if sheet.max_row > row_freeze and sheet.max_column > column_freeze:
            freeze_cell = sheet.cell(row=self._COUNT_HEADER_ROW + 1,
                                     column=self._COLUMN_WORDS + 1)
            sheet.freeze_panes = freeze_cell

    def add_words_position(self, sheet_name, region_name, color_text,
                           words_by_position):
        sheet = self.wb[sheet_name]
        column = sheet.max_column + 1
        self._write_column_name_for_region(sheet, row=self._ROW_START_HEADER,
                                           column=column,
                                           region_name=region_name)
        self._write_formula_count_words_in_top_x(sheet, column)
        cells_words = sheet[
            f"{get_column_letter(self._COLUMN_WORDS)}{self._ROW_START_WORDS}"
            f":{get_column_letter(self._COLUMN_WORDS)}{sheet.max_row}"]
        for row_list in cells_words:
            for word_cell in row_list:
                word_position = words_by_position.get(word_cell.value, {})
                position = word_position.get('Yandex',
                                             self._UNDEFINED_WORD_POSITION)
                try:
                    position = int(position)
                except ValueError:
                    pass
                sheet.cell(row=word_cell.row, column=column, value=position)
        self._set_column_style(sheet, column, color_text)

    def save_wb(self):
        for sheet in self.wb.worksheets:
            self._set_table_style(sheet)
            if self._new_file:
                self._freeze_panes(sheet)
        create_parent_path(self.filepath)
        self.wb.save(self.filepath)

    def close_wb(self):
        self.wb.close()
