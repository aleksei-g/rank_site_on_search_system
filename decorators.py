MAX_COUNT_ATTEMPTS = 3


def check_execution(reinit_checker_in_attempt=False, download_path=None):
    def decorator(func):
        def check_func(*args, **kwargs):
            attempt = 1
            while attempt <= MAX_COUNT_ATTEMPTS:
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(
                        f"In attempt {attempt} of {MAX_COUNT_ATTEMPTS}: "
                        f"function: \"{func.__name__}\" "
                        f"raise exception: \"{type(e)}\" "
                        f"Message: {getattr(e, 'msg', None)}"
                    )
                    if attempt == MAX_COUNT_ATTEMPTS:
                        args[0].quit()
                        raise e
                    elif reinit_checker_in_attempt:
                        args[0].quit()
                        args[0].__init__(download_path)
                    attempt += 1
        return check_func
    return decorator
