import json
import os
import sys
from copy import deepcopy
from pathlib import Path

from csv_reader import get_dict_words_position_from_csv
from decorators import check_execution
from excel_book import ExcelBook
from position_checker import PositionChecker

WORK_DIR = Path(os.path.dirname(sys.argv[0])).resolve()
CONFIG_FILEPATH = Path(os.getenv('CONFIG_FILEPATH', 'work_data/config.json'))
MAX_COUNT_WORDS_CHECKS = 100


def add_work_dir_to_filepath(filepath, work_dir=WORK_DIR):
    if not filepath.is_absolute():
        filepath = Path(work_dir) / filepath
    return filepath


DOWNLOAD_PATH = add_work_dir_to_filepath(
    Path(os.getenv('DOWNLOAD_PATH', 'downloads')))


def get_json(filepath):
    filepath = add_work_dir_to_filepath(filepath)
    with open(filepath) as json_file:
        return json.load(json_file)


def get_list_lines_from_file(filepath):
    filepath = add_work_dir_to_filepath(filepath)
    lines_list = []
    with open(filepath) as file_handler:
        for line in file_handler:
            lines_list.append(line.strip())
    return lines_list


def get_chunk_list(iterable, count):
    return [iterable[i:i + count] for i in range(0, len(iterable), count)]


def add_words_for_check(json_data):
    for url in json_data:
        all_words = set()
        for sheet in url['sheets']:
            words_in_sheet = list(
                set(get_list_lines_from_file(Path(sheet['words_filepath']))))
            sheet['words'] = words_in_sheet
            all_words.update(set(words_in_sheet))
        all_words = sorted(list(all_words))
        chunk_list_words = get_chunk_list(all_words, MAX_COUNT_WORDS_CHECKS)
        url['words_lists_in_parts'] = {}
        list_loaded_files = {}
        for part_num, words_part in enumerate(chunk_list_words, start=1):
            url['words_lists_in_parts'][part_num] = words_part
            list_loaded_files[part_num] = None
        for region in url['regions']:
            region['list_loaded_files'] = deepcopy(list_loaded_files)


@check_execution(reinit_checker_in_attempt=True, download_path=DOWNLOAD_PATH)
def get_csv_files_for_words(checker, json_data):
    for url in json_data:
        for region in url['regions']:
            for part_num, loaded_file in region['list_loaded_files'].items():
                if loaded_file:
                    continue
                words_list_in_part = url['words_lists_in_parts'][part_num]
                csv_file = checker.get_csv_file_with_position(
                    url['url'],
                    region['code'],
                    region['name'],
                    words_list_in_part,
                    part_num,
                )
                region['list_loaded_files'][part_num] = csv_file


def add_words_position_from_csv_files(json_data):
    for url in json_data:
        for region in url['regions']:
            words_by_position = {}
            for file in region['list_loaded_files'].values():
                words_by_position.update(get_dict_words_position_from_csv(file))
            region['words_by_position'] = words_by_position


def add_words_position(json_data):
    add_words_for_check(json_data)
    checker = PositionChecker(DOWNLOAD_PATH)
    get_csv_files_for_words(checker, json_data)
    checker.quit()
    add_words_position_from_csv_files(json_data)


def write_data_to_xls(json_data):
    for url in json_data:
        wb = ExcelBook(add_work_dir_to_filepath(Path(url['result_filepath'])),
                       url['sheets'])
        for sheet in url['sheets']:
            regions = sorted(
                url['regions'],
                key=lambda region: int(region['code']),
                reverse=True,
            )
            for words_for_region in regions:
                wb.add_words_position(
                    sheet_name=sheet['name'],
                    region_name=words_for_region['region_name_in_column'],
                    color_text=words_for_region['color_text'],
                    words_by_position=words_for_region['words_by_position'],
                )
        wb.save_wb()
        wb.close_wb()


if __name__ == '__main__':
    json_data = get_json(CONFIG_FILEPATH)
    add_words_position(json_data)
    write_data_to_xls(json_data)
